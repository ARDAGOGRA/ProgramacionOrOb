package maestria.informatica;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Camarero extends JDialog {
    private JPanel contentPane;
    private JTextField codigo;
    private JTextField nombre;
    private JTextField Apellido;
    private JButton adicionarButton;
    private JButton eliminarButton;
    private JTable table1;
    private JTextField apellido;
    private JTextField tel;
    private JTextField sueldo;
    private JButton buttonOK;
    private DefaultTableModel model;

    public Camarero() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Ingreso de Camareros / Camareros en Servicio");
        model = new DefaultTableModel();
        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Salario");
        model.addColumn("Telefono");
        table1.setModel(model);

        adicionarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int codVar = Integer.parseInt(codigo.getText());
                String nombreVar = nombre.getText();
                String apellidoVar = apellido.getText();
                int telVar = Integer.parseInt(tel.getText());
                double sueldoVar = Double.parseDouble(sueldo.getText());

                codigo.setText("");
                nombre.setText("");
                apellido.setText("");
                tel.setText("");
                sueldo.setText("");

                model.addRow(new Object[]{codVar, nombreVar, apellidoVar, sueldoVar, telVar});
                super.mouseClicked(e);
            }
        });
        eliminarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                model.removeRow(table1.getSelectedRow());
                super.mouseClicked(e);
            }
        });
    }
}
