package maestria.informatica;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Client extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JPanel panel1;
    private JTextField nit;
    private JTextField nombre;
    private JTextField apellido;
    private JTextField direccion;
    private JTextField tel;
    private DefaultTableModel model;
    private JTable table1;
    private JButton adicionarButton;
    private JButton eliminarButton;
    private JButton salirButton;

    public Client() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Ingreso de Nuevos Clientes");
        model = new DefaultTableModel();
        model.addColumn("Nit");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Dirección");
        model.addColumn("Teléfono");
        table1.setModel(model);

        adicionarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String nitVar = nit.getText();
                String nombreVar = nombre.getText();
                String apellidoVar = apellido.getText();
                String direccionVar = direccion.getText();
                int telVar = Integer.parseInt(tel.getText());

                nit.setText("");
                nombre.setText("");
                apellido.setText("");
                direccion.setText("");
                tel.setText("");

                model.addRow(new Object[]{nitVar, nombreVar, apellidoVar, direccionVar, telVar});
                super.mouseClicked(e);
            }
        });
        eliminarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                model.removeRow(table1.getSelectedRow());
                super.mouseClicked(e);
            }
        });
    }
}
