package maestria.informatica;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Coninero extends JDialog {
    private JPanel contentPane;
    private JTextField cod;
    private JButton adicionarButton;
    private JTextField nombre;
    private JTextField apellido;
    private JTextField tipo;
    private JTextField tel;
    private JTextField Sueldo;
    private JTable table1;
    private JButton eliminarButton;
    private JTextField sueldo;
    private JButton buttonOK;
    private DefaultTableModel model;

    public Coninero() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Ingreso de Cocinero / Cocineros en Servicio");
        model = new DefaultTableModel();
        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Tipo");
        model.addColumn("Telefono");
        model.addColumn("Sueldo");
        table1.setModel(model);

        adicionarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int codVar = Integer.parseInt(cod.getText());
                String nombreVar = nombre.getText();
                String apellidoVar = apellido.getText();
                String tipoVar = tipo.getText();
                int telVar = Integer.parseInt(tel.getText());
                double sueldoVar = Double.parseDouble(sueldo.getText());

                cod.setText("");
                nombre.setText("");
                apellido.setText("");
                tipo.setText("");
                tel.setText("");
                sueldo.setText("");

                model.addRow(new Object[]{codVar, nombreVar, apellidoVar, tipoVar, telVar, sueldoVar});
                super.mouseClicked(e);
            }
        });
        eliminarButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                model.removeRow(table1.getSelectedRow());
                super.mouseClicked(e);
            }
        });
    }
}
