package maestria.informatica;

import javax.swing.*;

public class Mesas extends JDialog {
    private JPanel contentPane;
    private JCheckBox fondoIzquierdaCheckBox;
    private JCheckBox fondoCentroCheckBox;
    private JCheckBox fondoDerechaCheckBox;
    private JCheckBox centroIzquierdaCheckBox;
    private JCheckBox centroCheckBox;
    private JCheckBox centroDerechaCheckBox;
    private JCheckBox frenteIzquierdaCheckBox;
    private JCheckBox fenteCentroCheckBox;
    private JCheckBox frenteDerechaCheckBox;
    private JButton buttonOK;

    public Mesas() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

    }
}
