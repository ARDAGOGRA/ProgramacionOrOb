package maestria.informatica;

import javax.swing.*;
import java.awt.event.*;

public class Principal extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton ingresarNuevoClienteButton;
    private JButton verMesasOcupadasDesocupadasButton;
    private JButton ingresoDeCamarerosCamarerosButton;
    private JButton ingresoDeCocinerosCocinerosButton;

    public Principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Restaurante La Prueba");
        setIconImage(new ImageIcon("/Mario-icon.png").getImage());
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
           }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ingresarNuevoClienteButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Client cli = new Client();
                cli.pack();
                cli.setVisible(true);
                super.mouseClicked(e);
            }
        });
        ingresoDeCocinerosCocinerosButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Coninero coc = new Coninero();
                coc.pack();
                coc.setVisible(true);
                super.mouseClicked(e);
            }
        });
        ingresoDeCamarerosCamarerosButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Camarero cam = new Camarero();
                cam.pack();
                cam.setVisible(true);
                super.mouseClicked(e);
            }
        });
        verMesasOcupadasDesocupadasButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Mesas mes = new Mesas();
                mes.pack();
                mes.setVisible(true);
                super.mouseClicked(e);
            }
        });
    }

    private void onOK() {
        // add your code here
        int opcion = JOptionPane.showConfirmDialog(getParent(),"¿Seguro/a que desea Salir?" , "Confirmacion", 0,2);
        if (opcion == JOptionPane.YES_OPTION)
            dispose();
    }

    private void onCancel() {
        // add your code here if necessary

    }

    public static void main(String[] args) {
        Principal dialog = new Principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
